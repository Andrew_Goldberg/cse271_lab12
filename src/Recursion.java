/**
 * This has some basic recursive functionallity.
 * 
 * @author Andrew Goldberg
 *
 */
public class Recursion {
    /**
     * This method tests the two functions inside of this class.
     * 
     */
    public static void main(String[] args) {
        System.out.println(powerN(3, 1));
        System.out.println(powerN(3, 2));
        System.out.println(powerN(3, 3));

        System.out.println(triangle(0));
        System.out.println(triangle(1));
        System.out.println(triangle(2));

    }

    /**
     * This is a recursive function to find the power of a number. It's base
     * case is when n<1.
     * 
     * @param int base The number to be multipled.
     * @param int n the current power. Gets redueced by 1 every time.
     * @return int the result of the power.
     */
    public static int powerN(int base, int n) {
        if (n < 1) {
            return 1;
        }
        return base * powerN(base, n - 1);
    }

    /**
     * This creates a triangle made of blocks where every row there is one more
     * block than the one before it. It is recursive with a base case of when
     * row is less than 1.
     * 
     * @param int row, how many rows are remaining. reduces by 1 with each row.
     * @return int the total amount of trinagles.
     */
    public static int triangle(int row) {
        if (row < 1) {
            return 0;
        }

        return row + triangle(row - 1);
    }
}
